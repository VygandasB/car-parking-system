<?php
include 'includes/db.php';
include 'parts/header.php';
include 'parts/footer.php';
?>
<!doctype HTML>
  <main>
    <legend>Pasirinkite mėnesį</legend>
    <select>
      <option value="sausis">Sausis</option>
      <option value="vasaris">Vasaris</option>
      <option value="kovas">Kovas</option>
      <option value="balandis">Balandis</option>
      <option value="geguze">Gegužė</option>
      <option value="birzelis">Birželis</option>
      <option value="liepa">Liepa</option>
      <option value="rugpjutis">Rugpjūtis</option>
      <option value="rugsejis">Rugsėjis</option>
      <option value="spalis">Spalis</option>
      <option value="lapkritis">Lapkritis</option>
      <option value="gruodis">Gruodis</option>
    </select>
    <table class="table">
      <thead>
        <tr>
          <th>Automobilių aikštelėje stovėjo automobilių</th>
          <th>Bendra surinkta pinigų suma</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>50</td>
          <td>1000€</td>
        </tr>
      </tbody>
    </table>
  </main>
  </body>
  </html>
