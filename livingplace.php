<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset ($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$name = isset($_GET['name']) ? $_GET['name'] : '';
if(!$name) {
  $name = isset($_POST['name']) ? $_POST['name'] : '';
}
$type = isset($_GET['type']) ? $_GET['type'] : '';
$currname = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE cities SET cname = :name, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':name', $name);
    $update->bindParam(':id', $id);
    $update->execute();
  }
  else {
    $send=$conn->prepare('INSERT INTO cities (cname, updated_at, created_at) VALUES (:name, NOW(), NOW() )');
    $send->bindParam(':name', $name);
    $send->execute();
  }
}
else if ($type == 'edit')
{
  $edit=$conn->prepare('SELECT id, cname FROM cities WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $name = $edit->fetch();
  $currname = $name['cname'];
}
if ($type == 'delete')
{
  $delete =$conn->prepare('DELETE FROM cities WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}
$null=0;
$sql = 'SELECT id, cname, updated_at, created_at
FROM cities
WHERE id>:id';
$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute(array(':id' => $null));
$dbname = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
  <main>
    <?php
    $SQL = "SELECT cname FROM cities";
    $strm = $conn->prepare($SQL);
    $strm->execute();
    $name = $strm->fetchAll();
    ?>
    <table class="table">
      <thead>
        <tr>
          <th>Miestas</th>
          <th>Redaguoti</th>
          <th>Ištrinti</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dbname as $key) {
          echo '<tr>
          <td class="cell">'.$key['cname'].'</td>
          <td class="table"><a href="livingplace.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
          <td class="table"><a href="livingplace.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
          </tr>';}
          ?>
      </tbody>
    </table>
  <form action="livingplace.php?id=<?php echo $id; ?>" method="POST">
    <?php
    if ($id) {
      echo '<input type="hidden" name="_method" value="PUT">';
    }
     ?>
    <fieldset>
      <legend>Gyvenamoji vieta</legend>
      <p><i class="fa fa-home"></i> Miestas:
          <input type="text" name="name" value="<?php echo $currname; ?>" /></p>
        <button type="submit" class="button" name="send" value="Ok">Irašyti</button>
      </fieldset>
    </form>
</main>
<?php
include 'parts/footer.php';
 ?>
</body>
</html>
