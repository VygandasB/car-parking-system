<?php
include 'includes/db.php';
include 'parts/header.php';
include 'parts/footer.php';
?>
<!doctype HTML>
  <main>
  <table class="table">
    <thead>
      <tr>
        <th>Valstybinis numeris</th>
        <th>Laikomi automobiliai</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>KRS-001</td>
        <td>Audi</td>
      </tr>
      <tr>
        <td>KRS-002</td>
        <td>McLaren P1</td>
      </tr>
      <tr>
        <td>KRS-004</td>
        <td>Lamborghini Huracán</td>
      </tr>
    </tbody>
  </table>
</main>
</body>
</html>
