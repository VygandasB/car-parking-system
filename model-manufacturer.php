<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset ($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$model = isset($_POST['model']) ? $_POST['model'] : '';
if(!$model) {
  $model = isset($_POST['model']) ? $_POST['model'] : '';
}
$name = isset($_POST['name']) ? $_POST['name'] : '';
if(!$name) {
  $name = isset($_POST['name']) ? $_POST['name'] : '';
}
$manufacturer_id = isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '';
if(!$manufacturer_id) {
  $manufacturer_id = isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '';
}

$type = isset($_GET['type']) ? $_GET['type'] : '';
$currmodel = '';
$currname = '';
$currmanufacturer_id = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE models SET name = :name, manufacturer_id = :manufacturer_id, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':name', $model);
    $update->bindParam(':id', $id);
    $update->bindParam(':manufacturer_id', $manufacturer_id);
    $update->execute();
  }
  else {
    $send=$conn->prepare('INSERT INTO models (name, manufacturer_id, updated_at, created_at) VALUES (:name,:manufacturer_id, NOW(), NOW() )');
    $send->bindParam(':name', $model);
    $send->bindParam(':manufacturer_id', $manufacturer_id);
    $send->execute();
  }
}
else if ($type == 'edit')
{
  $edit=$conn->prepare('SELECT id, name, manufacturer_id FROM models WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $model = $edit->fetch();
  $currmodel = $model['name'];
  $currmanufacturer_id = $model['manufacturer_id'];
}
if ($type == 'delete')
{
  $delete =$conn->prepare('DELETE FROM models WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}
$null=0;
$sql = 'SELECT m.id, m.name, manu.mname
FROM models as m INNER JOIN manufacturers as manu ON m.manufacturer_id=manu.id ';
$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute();
$dbmodel = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
  <main>
    <?php
    $SQL = "SELECT mname FROM manufacturers";
    $strm = $conn->prepare($SQL);
    $strm->execute();
    $manufacturer = $strm->fetchAll();
    ?>
  <table class="table">
    <thead>
      <tr>
        <th>Automobilio markė</th>
        <th>Saugomo automobilio gamykla</th>
        <th>Redaguoti</th>
        <th>Ištrinti</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php
        foreach ($dbmodel as $key) {
          echo '<tr>
          <td class="cell">'.$key['name'].'</td>
          <td class="cell">'.$key['mname'].'</td>
          <td class="table"><a href="model-manufacturer.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
          <td class="table"><a href="model-manufacturer.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
          </tr>';}
          ?>
      </tr>
    </tbody>
  </table>
  <form action="model-manufacturer.php?id=<?php echo $id; ?>" method="post">
    <?php
    if ($id) {
      echo '<input type="hidden" name="_method" value="PUT">';
    }
     ?>
    <fieldset>
      <legend>Automobilio Modelis</legend>
      <p><i class="fa fa-car"></i> Automobilio markė:
        <input type="text" name="model" value="<?php echo $currmodel; ?>" /></p>
        <p><i class="fa fa-car"></i> Automobilio gamykla:
          <?php
          $sql = "SELECT * FROM manufacturers ";
          $strm = $conn->prepare($sql);
          $strm->execute();
          $dbmanufacturers = $strm->fetchAll();

          echo '<select name="manufacturer_id">';
          foreach ($dbmanufacturers as $key)
          {
            $selected ='';
               if($currmanufacturer_id)
               {
                 if ($currmanufacturer_id == $key['id'])
                 {
                   $selected ='SELECTED';
                 }
                 else
                 {
                   $selected ='';
                 }
               }
            echo '<option '.$selected.' value="'.$key['id'].'">' .$key['mname'].'</option>';
          }
          echo '</select>';
          echo '</br>'
          ?>
        <button type="submit" class="button" name="send" value="Ok">Irašyti</button>
      </fieldset>
    </form>
</main>
</body>
</html>
