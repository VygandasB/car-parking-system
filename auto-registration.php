<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset ($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$car = isset($_POST['car']) ? $_POST['car'] : '';
if(!$car) {
  $car = isset($_POST['car']) ? $_POST['car'] : '';
}
$cnumber = isset($_POST['cnumber']) ? $_POST['cnumber'] : '';
if(!$cnumber ) {
  $cnumber = isset($_POST['cnumber']) ? $_POST['cnumber'] : '';
}

$model_id = isset($_POST['model_id']) ? $_POST['model_id'] : '';
if(!$cnumber ) {
  $model_id = isset($_POST['model_id']) ? $_POST['model_id'] : '';
}

$type = isset($_GET['type']) ? $_GET['type'] : '';
$currcnumber = '';
$currmodel_id = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE cars SET cnumber = :cnumber, model_id = :model_id, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':cnumber', $cnumber);
    $update->bindParam(':model_id', $model_id);
    $update->bindParam(':id', $id);
    $update->execute();
  }
  else {
    $send=$conn->prepare('INSERT INTO cars(cnumber, model_id, updated_at, created_at) VALUES (:cnumber, :model_id, NOW(), NOW() )');
    $send->bindParam(':cnumber', $cnumber);
    $send->bindParam(':model_id', $model_id);
    $send->execute();
  }
}
else if ($type == 'edit')
{
  $edit=$conn->prepare('SELECT id, cnumber, model_id FROM cars WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $registration = $edit->fetch();
  $currcnumber = $car['cnumber'];
  $currmodel_id = $car['model_id'];
}
if ($type == 'delete')
{
  $delete =$conn->prepare('DELETE FROM cars WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}
$null=0;
$sql = 'SELECT id, cnumber, model_id, updated_at, created_at
FROM cars
WHERE id>:id';
$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute(array(':id' => $null));
$dbcar = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
  <main>
  <table class="table">
    <thead>
      <tr>
        <th>Automobilio markė</th>
        <th>Valstybinis numeris</th>
        <th>Redaguoti</th>
        <th>Ištrinti</th>
      </tr>
    </thead>
  <tbody>
  <tr>
    <?php
    foreach ($dbcar as $key) {
      echo '<tr>
      <td class="cell">'.$key['model_id'].'</td>
      <td class="cell">'.$key['cnumber'].'</td>
      <td class="table"><a href="auto-registration.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
      <td class="table"><a href="auto-registration.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
      </tr>';}
      ?>
  </tr>
</tbody>
</table>
<form action="auto-registration.php?id=<?php echo $id; ?>" method="POST">
  <?php
  if ($id) {
    echo '<input type="hidden" name="_method" value="PUT">';
  }
   ?>
  <fieldset>
    <legend>Automobilio Registracija</legend>
    <p><i class="fa fa-car"></i> Automobilio markė:
      <?php
      $sql = "SELECT * FROM models ";
      $strm = $conn->prepare($sql);
      $strm->execute();
      $dbmodels = $strm->fetchAll();

      echo '<select name="name">';
      foreach ($dbmodels as $key)
      {
        echo '<option value="'.$key['id'].'">' .$key['name'].'</option>';
      }
      echo '</select>';
      echo '</br>'
      ?>
    <p><i class="fa fa-car"></i> Valstybinis numeris:
        <input type="text" name="cnumber" value="<?php echo $cnumber; ?>"  /></p>
        <input type="submit" value="Įrašyti">
  </fieldset>
</form>
</main>
<?php
include 'parts/footer.php';
?>
</body>
</html>
