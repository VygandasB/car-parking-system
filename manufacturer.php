<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset ($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$manufacturer = isset($_GET['manufacturer']) ? $_GET['manufacturer'] : '';
if(!$manufacturer) {
  $manufacturer = isset($_POST['manufacturer']) ? $_POST['manufacturer'] : '';
}
$type = isset($_GET['type']) ? $_GET['type'] : '';
$currmanufacturer = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE manufacturers SET mname = :name, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':name', $manufacturer);
    $update->bindParam(':id', $id);
    $update->execute();
  }
  else {
    $send=$conn->prepare('INSERT INTO manufacturers (mname, updated_at, created_at) VALUES (:name, NOW(), NOW() )');
    $send->bindParam(':name', $manufacturer);
    $send->execute();
  }
}
else if ($type == 'edit')
{
  $edit=$conn->prepare('SELECT id, mname FROM manufacturers WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $manufacturer = $edit->fetch();
  $currmanufacturer = $manufacturer['mname'];
}
if ($type == 'delete')
{
  $delete =$conn->prepare('DELETE FROM manufacturers WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}
$null=0;
$sql = 'SELECT id, mname, updated_at, created_at
FROM manufacturers
WHERE id>:id';
$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute(array(':id' => $null));
$dbmanufacturer = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
    <main>
      <?php
      $SQL = "SELECT mname FROM manufacturers";
      $strm = $conn->prepare($SQL);
      $strm->execute();
      $manufacturer = $strm->fetchAll();
      ?>
    <table class="table">
      <thead>
        <tr>
          <th>Saugomo automobilio gamykla</th>
          <th>Redaguoti</th>
          <th>Ištrinti</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dbmanufacturer as $key) {
          echo '<tr>
          <td class="cell">'.$key['mname'].'</td>
          <td class="table"><a href="manufacturer.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
          <td class="table"><a href="manufacturer.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
          </tr>';}
          ?>
      </tbody>
    </table>
    <form action="manufacturer.php?id=<?php echo $id; ?>" method="POST">
      <?php
      if ($id) {
        echo '<input type="hidden" name="_method" value="PUT">';
      }
       ?>
      <fieldset>
        <legend>Automobilio Gamykla</legend>
        <p><i class="fa fa-car"></i> Gamykla:
            <input type="text" name="manufacturer" value="<?php echo $currmanufacturer; ?>" /></p>
          <button type="submit" class="button" name="send" value="Ok">Irašyti</button>
        </fieldset>
      </form>
  </main>
  <?php
  include 'parts/footer.php';
   ?>
  </body>
  </html>
