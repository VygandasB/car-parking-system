<!doctype HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge">
  <title>Parkavimo Sistema</title>
  <meta name="description" content="Parkavimo sistema @parking.lt">
  <meta name="author" content="Vygandas">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <div id="container">
     <header>
           <nav>
              <ul>
                <li><a href="index.php">Pagrindinis</a></li>
                <li><a href="registration.php">Registracija</a></li>
                <li><a href="user-registration.php">Kliento registracija</a></li>
                <li><a href="auto-registration.php">Automobilio registracija</a></li>
                <li><a>Klasifikatoriai</a>
                   <ul>
                     <li><a href="livingplace.php">Gyvenamoji vieta</a></li>
                     <li><a href="manufacturer.php">Saugomo automobilio gamykla</a></li>
                     <li><a href="model-manufacturer.php">Automobilio markė ir gamykla</a></li>
                   </ul>
                </li>
                <li><a>Ataskaitos</a>
                   <ul>
                     <li><a href="clientcars-now.php">Dabar laikomi automobiliai</a></li>
                     <li><a href="clientcars.php">Kliento saugomi automobiliai</a></li>
                     <li><a href="car-history-and-money.php">Kokie automobiliai stovėjo ir kiek surinkta pinigų</a></li>
                   </ul>
                </li>
              </ul>
           </nav>
     </header>
     </html>
