<?php
include 'includes/db.php';
include 'parts/header.php';
include 'parts/footer.php';
?>
<!doctype HTML>
  <main>
    <legend>Pasirinkite klientą</legend>
    <select>
      <option value="Jonas">Jonas</option>
      <option value="Petras">Petras</option>
    </select>
    <table class="table">
      <thead>
        <tr>
          <th>Vardas</th>
          <th>Pavardė</th>
          <th>Telefonas</th>
          <th>Gyvenamoji vieta</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Jonas</td>
          <td>Jonaitis</td>
          <td>+37045521666</td>
          <td>Vilnius</td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead>
        <tr>
          <th>Valstybinis numeris</th>
          <th>Laikomi automobiliai</th>
          <th>Pastatymo data</th>
          <th>Išvykimo data</th>
        </tr>
      </thead>
      <tbody>
        <tr>
           <td>KRS-002</td>
           <td>Lamborghini Huracán</td>
           <td>2017/10/15</td>
           <td>2018/01/01</td>
         </tr>
         <tr>
           <td>KRS-003</td>
           <td>McLaren P1</td>
           <td>2017/10/14</td>
           <td>2017/11/09</td>
         </tr>
       </tbody>
     </table>
   </main>
   </body>
   </html>
