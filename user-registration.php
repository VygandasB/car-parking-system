<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$user = isset($_POST['user']) ? $_POST['user'] : '';
if(!$user) {
  $user = isset($_POST['user']) ? $_POST['user'] : '';
}
$name = isset($_POST['name']) ? $_POST['name'] : '';
if(!$name) {
  $name = isset($_POST['name']) ? $_POST['name'] : '';
}
$lname = isset($_POST['lname']) ? $_POST['lname'] : '';
if(!$lname) {
  $lname = isset($_POST['lname']) ? $_POST['lname'] : '';
}
$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
if(!$phone) {
  $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
}
$city_id = isset($_POST['city_id']) ? $_POST['city_id'] : '';
if(!$city_id) {
  $city_id = isset($_POST['city_id']) ? $_POST['city_id'] : '';
}
$type = isset($_GET['type']) ? $_GET['type'] : '';
$currname = '';
$currlname = '';
$currphone = '';
$currcity_id = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE users SET name = :name, lname = :lname, phone = :phone, city_id = :city_id, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':name', $name);
    $update->bindParam(':lname', $lname);
    $update->bindParam(':phone', $phone);
    $update->bindParam(':city_id', $city_id);
    $update->bindParam(':id', $id);
    $update->execute();
  }
  else {

    $send=$conn->prepare('INSERT INTO users(name,lname,phone,city_id,updated_at,created_at) VALUES (:name,:lname,:phone,:city_id, NOW(), NOW() )');
    $send->bindParam(':name', $name);
    $send->bindParam(':lname', $lname);
    $send->bindParam(':phone', $phone);
    $send->bindParam(':city_id', $city_id);
    $send->execute();

  }
}
else if ($type == 'edit')
{
  $edit = $conn ->prepare('SELECT id, name, lname, phone, city_id FROM users WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $user = $edit->fetch();
  $currname = $user['name'];
  $currlname = $user['lname'];
  $currphone = $user['phone'];
  $currcity_id = $user['city_id'];

}
if ($type == 'delete')
{
  echo 'delete'.$id;
  $delete =$conn->prepare('DELETE FROM users WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}

$null=0;
$sql = 'SELECT us.id, us.name, us.lname, us.phone, ct.cname
FROM users as us
JOIN cities as ct on us.city_id = ct.id';

$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute();
$dbusers = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
  <main>
  <table class="table">
    <thead>
      <tr>
        <th>Vardas</th>
        <th>Pavardė</th>
        <th>Telefonas</th>
        <th>Gyvenamoji vieta</th>
        <th>Redaguoti</th>
        <th>Ištrinti</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php
        foreach ($dbusers as $key) {
          echo '<tr>
          <td class="cell">'.$key['name'].'</td>
          <td class="cell">'.$key['lname'].'</td>
          <td class="cell">'.$key['phone'].'</td>
          <td class="cell">'.$key['cname'].'</td>
          <td class="table"><a href="user-registration.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
          <td class="table"><a href="user-registration.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
          </tr>';}
          ?>
      </tr>
    </tbody>
  </table>
    <form action="user-registration.php?id=<?php echo $id; ?>" method="POST">
      <?php
      if ($id) {
        echo '<input type="hidden" name="_method" value="PUT">';
      }
       ?>
      <fieldset>
        <legend>Kliento registracija</legend>
        <p><i class="fa fa-book"></i> Vardas:
          <input type="text" name="name" value="<?php echo $currname; ?>"/></p>
          <p><i class="fa fa-book"></i> Pavardė:
            <input type="text" name="lname" value="<?php echo $currlname; ?>" /></p>
            <p><i class="fa fa-mobile"></i> Telefonas:
              <input type="text" name="phone" value="<?php echo $currphone; ?>" /></p>

                <?php
                $sql = "SELECT * FROM cities ";
                $strm = $conn->prepare($sql);
                $strm->execute();
                $dbcities = $strm->fetchAll();

                echo '<p><i class="fa fa-home"></i> Gyvenamoji vieta: </p>';
                echo '<select name="city_id">';
                foreach ($dbcities as $key)
                {
                  echo '<option value="'.$key['id'].'">' .$key['cname'].'</option>';
                }
                echo '</select>';
                echo '</br>'
                ?>

            <button type="submit" class="button" name="send" value="Ok">Toliau</button>
      </fieldset>
    </form>
</main>
<?php
include 'parts/footer.php';
?>
</body>
</html>
