<?php
include 'includes/db.php';
include 'parts/header.php';

$id = isset ($_GET['id']) ? $_GET['id'] : '';
if (!$id) {
  $id = isset($_POST['id']) ? $_POST['id'] : '';
}
$registration = isset($_POST['registration']) ? $_POST['registration'] : '';
if(!$registration) {
  $registration = isset($_POST['registration']) ? $_POST['registration'] : '';
}
$park_date = isset($_POST['park_date']) ? $_POST['park_date'] : '';
if(!$park_date ) {
  $park_date = isset($_POST['park_date']) ? $_POST['park_date'] : '';
}
$leave_date = isset($_POST['leave_date']) ? $_POST['leave_date'] : '';
if(!$leave_date) {
  $leave_date = isset($_POST['leave_date']) ? $_POST['leave_date'] : '';
}
$cost = isset($_POST['cost']) ? $_POST['cost'] : '';
if(!$cost) {
  $cost = isset($_POST['cost']) ? $_POST['cost'] : '';
}
$cnumber_id = isset($_POST['cnumber_id']) ? $_POST['cnumber_id'] : '';
if(!$cnumber_id) {
  $cnumber_id = isset($_POST['cnumber_id']) ? $_POST['cnumber_id'] : '';
}

$type = isset($_GET['type']) ? $_GET['type'] : '';
$currparkdate = '';
$currleavedate = '';
$currcost = '';
$currcnumber_id = '';
$method = isset($_POST['_method']) ? $_POST['_method'] : '';
$save = isset($_POST['send']) ? true : false;

if ($save)
{
  if ($method == 'PUT')
  {
    $update=$conn->prepare('UPDATE registrations SET park_date = :park_date,leave_date = :leave_date,cost = :cost, cnumber_id = :cnumber_id, updated_at = NOW() WHERE id=:id');
    $update->bindParam(':park_date', $park_date);
    $update->bindParam(':cnumber_id', $cnumber_id);
    $update->bindParam(':leave_date', $leave_date);
    $update->bindParam(':cost', $cost);
    $update->bindParam(':id', $id);
    $update->execute();
  }
  else {
    $send=$conn->prepare('INSERT INTO registrations(park_date, leave_date, cost, cnumber_id, updated_at, created_at) VALUES (:park_date, :leave_date, :cost, :cnumber_id, NOW(), NOW() )');
    $send->bindParam(':park_date', $park_date);
    $send->bindParam(':cnumber_id', $cnumber_id);
    $send->bindParam(':leave_date', $leave_date);
    $send->bindParam(':cost', $cost);
    $send->execute();
  }
}
else if ($type == 'edit')
{
  $edit=$conn->prepare('SELECT id, park_date, leave_date, cost, cnumber_id FROM registrations WHERE id=:id');
  $edit->bindParam(':id', $id);
  $edit->execute();
  $registration = $edit->fetch(PDO::FETCH_ASSOC);
  $currcnumber_id = $registration['cnumber_id'];
  $currparkdate = $registration['park_date'];
  $currleavedate = $registration['leave_date'];
  $currcost = $registration['cost'];
}
if ($type == 'delete')
{
  $delete =$conn->prepare('DELETE FROM registrations WHERE id=:id');
  $delete->bindParam(':id', $id);
  $delete->execute();
}
$null=0;

$sql = 'SELECT reg.id, reg.park_date, reg.leave_date, reg.cost, car.number
FROM registrations as reg INNER JOIN cars as car ON reg.cnumber_id=car.id ';

$strm = $conn ->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
$strm -> execute(array(':id' => $null));
$dbregistration = $strm->fetchALL(PDO::FETCH_ASSOC);
?>
<!doctype HTML>
    <main>
    <table class="table">
      <thead>
        <tr>
          <th>Automobilio numeris</th>
          <th>Pastatymo laikas</th>
          <th>Išvykimo laikas</th>
          <th>Suma</th>
          <th>Redaguoti</th>
          <th>Ištrinti</th>
        </tr>
      </thead>
          <tbody>
            <tr>
              <?php
              foreach ($dbregistration as $key) {
                echo '<tr>
                <td class="cell">'.$key['number'].'</td>
                <td class="cell">'.$key['park_date'].'</td>
                <td class="cell">'.$key['leave_date'].'</td>
                <td class="cell">'.$key['cost'].'</td>
                <td class="table"><a href="registration.php?type=edit&id='.$key['id'].'"><i class="fa fa-cog"></a></td>
                <td class="table"><a href="registration.php?type=delete&id='.$key['id'].'"><i class="fa fa-trash-o"></i></a></td>
                </tr>';}
                ?>
            </tr>
          </tbody>
        </table>
        <form action="registration.php?id=<?php echo $id; ?>" method="POST">
          <?php
          if ($id) {
            echo '<input type="hidden" name="_method" value="PUT">';
          }
           ?>
          <fieldset>
            <legend>Registracija</legend>
            <p><i class="fa fa-car"></i> Automobilio numeris:



              <?php
              $sql = "SELECT * FROM cars ";
              $strm = $conn->prepare($sql);
              $strm->execute();
              $dbcars = $strm->fetchAll();

              echo '<select name="cnumber_id">';
              foreach ($dbcars as $key)
              {
                $selected ='';
                   if($currcnumber_id)
                   {
                     if ($currcnumber_id == $key['id'])
                     {
                       $selected ='SELECTED';
                     }
                     else
                     {
                       $selected ='';
                     }
                   }
                echo '<option '.$selected.' value="'.$key['id'].'">' .$key['number'].'</option>';
              }
              echo '</select>';
              echo '</br>'
              ?>








            <p><i class="fa fa-book"></i> *Pastatymo laikas:
              <input type="date" name="park_date" value="<?php echo $currparkdate; ?>" required /></p>
            <p><i class="fa fa-book"></i> *Išvykimo laikas:
                <input type="date" name="leave_date" value="<?php echo $currleavedate; ?>" required /></p>
                <p><i class="fa fa-money"></i> Suma:
                    <input type="text" name="cost" value="<?php echo $currcost; ?>" /></p>
                <button type="submit" class="button" name="send" value="Ok">Toliau</button>
          </fieldset>
        </form>
        </main>
        <?php
        include 'parts/footer.php';
         ?>
      </body>
      </html>
